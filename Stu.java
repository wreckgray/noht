import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;
public class Stu extends JFrame implements ActionListener
{
JLabel l2 = new JLabel("Name:");
JLabel l3 = new JLabel("Age:");
JLabel l4 = new JLabel("Sex(M/F):");
JLabel l5 = new JLabel("Address:");
JLabel l6 = new JLabel("Course:");
JLabel l7 = new JLabel("Semester");
JTextField t1 = new JTextField();
JTextField t2 = new JTextField();
JCheckBox c1 = new JCheckBox("Male");
JCheckBox c2 = new JCheckBox("Female");
ButtonGroup bg = new ButtonGroup();
JComboBox c = new JComboBox();
JComboBox d = new JComboBox();
JComboBox e = new JComboBox();
JButton b = new JButton("Save");

public Stu()
{
this.setLayout(null);
this.setVisible(true);
this.setSize(1600,1080);

l2.setBounds(100,80,200,40);
l3.setBounds(100,160,200,40);
l4.setBounds(100,240,200,40);
l5.setBounds(100,320,200,40);
l6.setBounds(100,400,200,40);
l7.setBounds(100,480,200,40);
add(l2);
add(l3);
add(l4);
add(l5);
add(l6);
add(l7);
t1.setBounds(300,80,150,40);
add(t1);
c.setBounds(300,160,120,30);
for(int i=16;i<25;i++)
c.addItem(i);
add(c);
c1.setBounds(300,240,100,40);
c2.setBounds(400,240,100,40);
add(c1);
add(c2);
bg.add(c1);
bg.add(c2);
t2.setBounds(300,320,150,60);
add(t2);
d.setBounds(300,400,130,30);
d.addItem("B.Tech CSE");
d.addItem("B.Tech SWE");
d.addItem("B.Tech IT");
d.addItem("B.Tech Civil");
d.addItem("B.Tech Mech");
add(d);
e.setBounds(300,480,120,30);
for(int i=1;i<9;i++)
e.addItem(i);
add(e);
b.setBounds(300,540,120,50);
add(b);
b.addActionListener(this);
}
public static void main(String[] args)
{
JFrame jframe = new JFrame("JFrame Background Color");

    jframe.getContentPane().setBackground(Color.red);
new Stu();
}
public void actionPerformed(ActionEvent ar)
{
if(ar.getSource()==b)
{
this.hide();
}
}	
}